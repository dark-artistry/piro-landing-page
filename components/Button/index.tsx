import clsx from 'clsx';
import React from 'react'
import styles from './button.module.scss'

interface ButtonProps {
  children?: React.ReactNode,
  variant?: string,
  onClick?: () => void,
  href?: string,
  className?: string
}

const Button: React.FC<ButtonProps> = ({
  children,
  variant,
  onClick,
  href,
  className
}) => {

  const ActionTag = href ? "a" : "button";

  return (
    <ActionTag
      className={clsx(className, styles.button, variant && styles[variant])}
      onClick={onClick}
      href={href}
    >
      {children}
    </ActionTag>
  )
}

export default Button
