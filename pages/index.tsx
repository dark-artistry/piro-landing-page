import type { NextPage } from 'next'
import Layout from '../layouts'
import HomeView from '../views/HomeView'

const Home: NextPage = () => {
  return (
    <Layout>
      <HomeView />
    </Layout>
  )
}

export default Home
