import Image from 'next/image';
import React from 'react';
import Button from '../../../components/Button';
import styles from './licenced.module.scss'

const Licenced: React.FC = () => {
  return (
    <div className="container">
      <div className={styles.licenced}>
        <div className={styles.bgBlue}></div>
        <div className={styles.wrapper}>
          <div className={styles.words}>
            <h1>Terlisensi dan diawasi oleh Bank Indonesia sehingga transaksi aman.</h1>
            <h3>Dengan menggunakan Piro, segala kebutuhan transfermu dapat diproses dengan lebih mudah, aman, dan nyaman.</h3>
          </div>
          <div className={styles.illustration}>
            <Image
              src="/assets/licenced/illustration.png"
              width={600}
              height={500}
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Licenced
