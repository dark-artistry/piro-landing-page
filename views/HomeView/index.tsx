import React from 'react';
import FeeInfo from './FeeInfo';
import Featured from './Featured';
import Hero from './Hero';
import Licenced from './Licenced';

const HomeView: React.FC = () => {
  return (
    <>
      <Hero />
      <Licenced />
      <Featured />
      <FeeInfo />
    </>
  )
}

export default HomeView
