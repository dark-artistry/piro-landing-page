import Image from 'next/image';
import React from 'react';
import Button from '../../../components/Button';
import styles from './hero.module.scss'

const Hero: React.FC = () => {
  return (
    <div className="container">
      <div className={styles.hero}>
        <div className={styles.wrapper}>
          <div className={styles.words}>
            <h1>Mau transfer simpel<br />dengan biaya<br />terjangkau?</h1>
            <Button
              variant="primary"
              href="https://wani.piro.id/login"
            >
              Daftar Sekarang
            </Button>
          </div>
          <div className={styles.illustration}>
            <Image
              src="/assets/hero/illustration.png"
              width={485}
              height={500}
              alt=""
            />
          </div>
        </div>
        <Image
          className={styles.bgImg}
          src="/assets/hero/bg-blue.svg"
          layout="fill"
          objectFit="cover"
          objectPosition="right"
          alt=""
        />
      </div>
    </div>
  )
}

export default Hero
