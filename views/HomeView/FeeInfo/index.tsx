import Image from 'next/image';
import React from 'react';
import Button from '../../../components/Button';
import styles from './fee.module.scss'

const FeeInfo: React.FC = () => {
  return (
    <div className="container">
      <div className={styles.feeInfo}>
        <Image
          className={styles.bgImg}
          src="/assets/feeInfo/bg-blue.svg"
          layout="fill"
          objectFit='contain'
          objectPosition="left"
          alt=""
        />
        <div className={styles.wrapper}>
          <div className={styles.illustration}>
            <Image
              src="/assets/feeInfo/illustration.png"
              width={430}
              height={350}
              alt=""
            />
          </div>
          <div className={styles.words}>
            <h1>Pengenaan biaya layanan:</h1>
            <Button
              className="mb-10"
            >
              Kirim Uang
            </Button>
            <div style={{ display: "flex", justifyContent: 'center',alignItems: "center"}}>
            <div className={styles.fees}>
              <div className={styles.fee}>
                <h5>Virtual account</h5>
                <ul>
                  <li>Rp3.250</li>
                </ul>
              </div>
            </div>
            <div className={styles.fees}>
              <div className={styles.fee}>
                <h5>API Bank</h5>
                <ul>
                  <li>Rp2.000</li>
                </ul>
              </div>
            </div>
            </div>
            
            
          </div>
        </div>
      </div>
    </div>
  )
}

export default FeeInfo
