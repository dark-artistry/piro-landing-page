import React from 'react'
import Marquee from 'react-fast-marquee';
import Footer from './Footer';
import Header from './Header';

interface LayoutProps {
  children?: React.ReactNode;
}


const Layout: React.FC<LayoutProps> = ({
  children
}) => {
  return (
    <div>
      <Header />
      <div style={{position: "fixed", top: 0}}>
       <Marquee speed={50} direction={'left'} gradient={false}>
        <p style={{color: "red"}}> Piro.id saat ini dalam pengajuan perizinan izin Transfer Dana kepada Bank Indonesia. Seluruh data ini merupakan data dummy dan tidak dapat beroperasi sebelum memiliki izin efektif dari Bank Indonesia. </p>
      </Marquee>
      </div>
      {children}
      <Footer />
    </div>
  )
}

export default Layout
