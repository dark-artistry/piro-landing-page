import clsx from 'clsx'
import React from 'react'
import styles from './footer.module.scss'

const Footer: React.FC = () => {
  return (
    <div className={styles.footer}>
      <div className="container">
        <div className={styles.wrapper}>
          <div className={styles.logo}>
            <img src="/assets/logo.svg" />
          </div>
          <div className={clsx(styles.contact, "mr-[60px]")}>
            <h1>Jam layanan:</h1>
            <h5>Setiap hari:</h5>
            <h5>09.00 - 16.00 WIB</h5>
          </div>
          <div className={styles.contact}>
            <h1>Kontak Kami:</h1>
            <h5>admin@piro.id</h5>
            <h5>021-3973-7476</h5>
            <h5>Millenium Centennial Center Level 38</h5>
            <h5>Jl. Jend Sudirman Kav 25, Jakarta Selatan</h5>
            <h5>DKI Jakarta</h5>
          </div>
         
        </div>
      </div>
    </div>
  )
}

export default Footer
