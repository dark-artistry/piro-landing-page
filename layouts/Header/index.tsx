import Image from 'next/image'
import React from 'react'
import Marquee from 'react-fast-marquee';
import Button from '../../components/Button'
import styles from './header.module.scss'

const Header: React.FC = () => {
  return (
    <div className="container">
      <div className={styles.header}>
        <div className={styles.wrapper}>
          <div className={styles.logo}>
            <img
              src="/assets/logo.svg"
              className={styles.logoImg}
            />
          </div>
          
          <div className={styles.menu}>
            <Button
              variant="primary"
              href="https://wani.piro.id/login"
            >
              Masuk
            </Button>
            <Button
              variant="base"
              href="https://wani.piro.id/register"
            >
              Daftar
            </Button>
          </div>
        </div>
        
     
        <img
          src="/assets/header/bg-blue.svg"
          className={styles.bgImg}
        />
      </div>
    </div>
  )
}

export default Header
